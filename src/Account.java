public abstract class Account {
    private double balance;
    private String name;
    private static double interestRate;

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public Account(String name, double balance){
        this.balance = balance;
        this.name = name;
    }

    public Account(){
        this.name = "nimit";
        this.balance = 50;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public boolean withdraw(double amt){
        if (this.balance >= 0 && this.balance >= amt){
            this.balance -= amt;
            return true;
        } else {
            return false;
        }

    }
    public boolean withdraw() {
        return this.withdraw(20);
    }
}
