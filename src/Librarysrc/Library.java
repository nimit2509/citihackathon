package Librarysrc;
import java.util.HashMap;

public class Library {
    private HashMap<LibraryContent, Integer> bookList = new HashMap<>();
//    private HashMap<String, Integer> videoList = new HashMap<>();
    private String name;

    public Library(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void add(LibraryContent book){
        book.addToLib(bookList);
    }
//    public void add(Video video){
//        video.addToLib(videoList);
//    }

    public Borrowable borrow(Borrowable book){
        Borrowable.borrowFromLib(bookList,book);
        return book;
    }

    public void returnToLib(LibraryContent book){
        book.returnToLib(bookList);
    }
//    public Video borrow(Video video){
//        video.borrowFromLib(videoList);
//        return video;
//    }

}
