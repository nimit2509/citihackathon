package Librarysrc;

import java.util.HashMap;

public class Video extends LibraryContent implements Borrowable {
    private String maker;

    public Video(String name, String maker) {
        super(name);
        this.maker = maker;

    }
}
