package Librarysrc;

import java.util.HashMap;

public class Book extends LibraryContent implements Borrowable{
    private String name;
    private String author;

    public Book(String name, String author){
        super(name);
        this.author = author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getAuthor() {
        return author;
    }


}
