package Librarysrc;
import java.util.HashMap;
public abstract class LibraryContent {
    private String name;

    public LibraryContent(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public  void addToLib(HashMap<LibraryContent,Integer>  contentList){
        if ( contentList.containsKey(this)){
      System.out.println("Item is already there and one more is added");
             contentList.replace(this,  contentList.get(this) + 1);
        } else {
      System.out.println("item is new ");
             contentList.put(this, 1);
        }
    }
    public void borrowFromLib(HashMap<LibraryContent ,Integer> bookList, Borrowable borrowable){
        Borrowable.borrowFromLib(bookList, borrowable);
//        if (bookList.containsKey(getName())){
//            if (bookList.get(getName()) > 1){
//        System.out.println("more than one exists so supply is good");
//                bookList.replace(getName(), bookList.get(getName()) - 1);
//            } else {
//        System.out.println("you are getting the last one");
//                bookList.remove(getName());
//            }
//
//        }
    }
    public void returnToLib(HashMap<LibraryContent,Integer> bookList){
        if (!bookList.containsKey(this)){
            bookList.put(this, 1);
        } else {bookList.put(this, bookList.get(this) + 1);}
        System.out.println("you have returned the item");

    }

}
