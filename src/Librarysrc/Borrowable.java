package Librarysrc;

import java.util.HashMap;

public interface Borrowable {



    static void borrowFromLib(HashMap<LibraryContent, Integer> bookList, Borrowable book) {
        if (bookList.containsKey(book)) {
            if (bookList.get(book) > 1) {
                System.out.println("more than one exists so supply is good");
                bookList.replace((LibraryContent) book, bookList.get(book) - 1);
            } else {
                System.out.println("you are getting the last one");
                bookList.remove(book);
            }

        }
    }
}
